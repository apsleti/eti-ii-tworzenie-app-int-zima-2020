<h1>Rejestracja nowego użytkownika</h1>

<?php
$username = $password = $repeatPassword = '';
if (isset($formData['username'])) {
    $username = $formData['username'];
}
/** @var $router \App\Router */
?>
<form action="<?= $router->generate('register'); ?>" method="post">

    <?php if (isset($errors['username'])): ?>
        <ul>
            <?php foreach ($errors['username'] as $error) : ?>
                <li><?= $error ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <input name="registration[username]" value="<?= $username ?>" placeholder="Twoja nazwa użytkownika"/>

    <?php if (isset($errors['password'])): ?>
        <ul>
            <?php foreach ($errors['password'] as $error) : ?>
                <li><?= $error ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <input name="registration[password]" type="password" value="" placeholder="Hasło"/>

    <?php if (isset($errors['repeatPassword'])): ?>
        <ul>
            <?php foreach ($errors['repeatPassword'] as $error) : ?>
                <li><?= $error ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <input name="registration[repeatPassword]" type="password" value="" placeholder="Powtórz hasło"/>

    <button type="submit">Zarejestruj</button>
</form>