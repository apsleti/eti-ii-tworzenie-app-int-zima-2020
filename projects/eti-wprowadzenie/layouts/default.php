<html>
    <head>
        <title>Blog App</title>
    </head>

    <body>
    <nav>
        <?php /** @var App\Router $router */ ?>
        <ul>
            <?php if (!$session->has('user')): ?>
                <li><a href="<?= $router->generate('register') ?>">Rejestracja</a></li>
            <?php endif; ?>
            <li><a href="<?= $router->generate('homepage') ?>">Homepage</a></li>
            <li><a href="<?= $router->generate('blog', ['id' => 3]) ?>">Blog</a></li>
        </ul>
    </nav>
    <h1>Hello World App!</h1>

        <?php include 'addons/flash-messages.php' ?>
        <?php if ($session->has('user')): ?>
            <p>Witaj <?= $session->get('user') ?></p>

            <p>
                <a href="<?= $router->generate('logout') ?>">Wyloguj</a>
            </p>
        <?php else: ?>
            <form action="<?= $router->generate('login_check') ?>" method="post">
                <input name="username" value="" placeholder="Username" />
                <input name="password" value="" type="password" placeholder="User Password">

                <button type="submit">Zaloguj mnie</button>
            </form>
        <?php endif; ?>


        <div>
            <?php echo $content ?>
        </div>
    </body>
</html>
