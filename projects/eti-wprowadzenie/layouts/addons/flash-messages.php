<?php if ($session->hasFlashMessages()): ?>
    <?php foreach ($session->getFlashMessages() as $type => $message): ?>
        <ul class="flash-message flash-message--<?= $type ?>">
            <?php foreach ($message as $message) : ?>
                <li> <?= $message ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endforeach; ?>
<?php endif; ?>