<html>
    <head>
        <title>Error Occured</title>
    </head>

    <body>
        <h1>Hello World App! ERROR LAYOUT</h1>
        <?php include 'addons/flash-messages.php' ?>

        <?php /** @var App\Router $router */ ?>

        <a href="<?= $router->generate('homepage') ?>">Homepage</a>
        <a href="<?= $router->generate('blog', ['id' => 3]) ?>">Blog</a>
        <a href="<?= $router->generate('user') ?>">User</a>

        <div>
            <?php echo $content ?>
        </div>
    </body>
</html>
