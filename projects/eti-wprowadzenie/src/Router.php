<?php

namespace App;

use App\Controllers\ControllerInterface;
use App\Controllers\ErrorController;
use App\Exception\PageNotFoundException;
use App\Response\ErrorResponse;
use App\Exception\InvalidRouteException;

/**
 * Class Router
 */
class Router
{
    /**
     * @var array
     */
    private $routes;

    /**
     * Router constructor.
     * @param array $routes
     */
    public function __construct(array $routes = [])
    {
        $this->routes = $routes;
    }

    /**
     * Dopasowuje URL do istniejących routingów.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function match(Request $request)
    {
        $trimmedRequestPath = ltrim($request->getPath(), '/');
        $requestPathSegments = explode('/', $trimmedRequestPath);

        foreach ($this->routes as $routeName => $routeConfig) {
            $trimmedRoute = ltrim($routeConfig['path'], '/');
            $routeSegments = explode('/', $trimmedRoute);

            $params = $this->checkRoute($routeSegments, $requestPathSegments);
            if ($params !== false) {

                $request->setParameters($params);
                $controllerFactory = $routeConfig['controller'] ?? null;

                if (is_callable($controllerFactory)) {
                    return $controllerFactory();
                } elseif ($controllerFactory instanceof ControllerInterface) {
                    return $routeConfig['controller'];
                }

                throw new InvalidRouteException($routeConfig['path']);
            }
        }

        throw new PageNotFoundException();
    }

    /**
     * @param string $name
     * @param array $routeConfig
     */
    public function addRoute(string $name, array $routeConfig)
    {
        $this->routes[$name] = $routeConfig;
    }

    /**
     * @param $name
     * @param array $params
     * @return string
     * @throws \Exception
     */
    public function generate($name, $params = [])
    {
        if (!isset($this->routes[$name])) {
            throw new \Exception(sprintf('Route "%s" does not exists', $name));
        }

        $path = $this->routes[$name]['path'];
        $trimmedRoute = ltrim($path, '/');
        $routeSegments = explode('/', $trimmedRoute);

        $uri = [];
        for ($i = 0; $i < count($routeSegments); $i++) {
            if (preg_match('/^{(.*)}$/', $routeSegments[$i], $matches)) {
                $uri[] = $params[$matches[1]];

            } else {
                $uri[] = $routeSegments[$i];
            }
        }

        return '/' . implode('/', $uri);
    }

    /**
     * @param array $routeSegments
     * @param array $requestPathSegments
     * @return array|false
     */
    private function checkRoute(array $routeSegments, array $requestPathSegments)
    {
        $params = [];

        for ($i = 0; $i < count($routeSegments); $i++) {
            if (preg_match('/^{(.*)}$/', $routeSegments[$i], $matches)) {
                $params[$matches[1]] = $requestPathSegments[$i];
            } elseif ($routeSegments[$i] !== $requestPathSegments[$i]) {
                return false;
            }
        }

        return $params;
    }
}