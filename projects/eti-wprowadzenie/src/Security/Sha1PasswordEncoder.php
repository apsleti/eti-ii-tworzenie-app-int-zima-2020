<?php

namespace App\Security;

class Sha1PasswordEncoder implements PasswordEncoderInterface
{
    public function encodePassword(string $plainPassword): string
    {
        return sha1($plainPassword);
    }
}