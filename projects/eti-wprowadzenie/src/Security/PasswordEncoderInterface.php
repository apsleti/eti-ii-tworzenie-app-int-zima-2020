<?php

namespace App\Security;

use App\Model\UserCredentials;

interface PasswordEncoderInterface
{
    public function encodePassword(string $plainPassword): string;
}