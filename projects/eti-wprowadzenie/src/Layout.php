<?php

namespace App;

class Layout
{
    /**
     * @var string
     */
    private $page;
    /**
     * @var string
     */
    private $layoutName;

    /**
     * @var string
     */
    private $title = 'APSL Page Title';

    /**
     * @var array
     */
    private $params;

    /**
     * Layout constructor.
     * @param string $page
     * @param string $layoutName
     * @param array $params
     */
    public function __construct(string $page, string $layoutName, array $params)
    {
        $this->page = $page;
        $this->layoutName = $layoutName;
        $this->params = $params;
    }

    public function render(): string
    {
        extract(array_merge($this->params, [
            'content' => $this->renderPageContent(),
            'session' => ServiceContainer::getInstance()->get('session')
        ]));
        ob_start();
        include __DIR__ . "/../layouts/{$this->layoutName}.php";

        return ob_get_clean();
    }

    private function renderPageContent()
    {
        ob_start();
        extract($this->params);
        include __DIR__ . "/../templates/{$this->page}.php";
        return ob_get_clean();
    }
}
