<?php

namespace App\Exception;

class InvalidRouteException extends ServerErrorException
{
    public function __construct(string $routePath)
    {
        parent::__construct(sprintf('Invalid route configuration for "%s".', $routePath));
    }
}