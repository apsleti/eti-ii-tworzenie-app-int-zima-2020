<?php

namespace App\Services;

use App\Exception\UserExistsExcetpion;
use App\Model\UserCredentials;
use App\Repository\UserRepositoryInterface;
use App\Security\PasswordEncoderInterface;

/**
 * Class UserManager
 */
class UserManager
{
    /**
     * @var PasswordEncoderInterface
     */
    private PasswordEncoderInterface $passwordEncoder;

    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * UserManager constructor.
     * @param UserRepositoryInterface $userRepository
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepositoryInterface $userRepository, PasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param string $userName
     * @param string $plainPassword
     * @throws \Exception
     */
    public function registerUser(string $userName, string $plainPassword)
    {
        $userCredentials = $this->userRepository->findCredentialsByUsername($userName);
        if ($userCredentials instanceof UserCredentials) {
            throw new UserExistsExcetpion("User with name {$userName} already exists!");
        }

        $credentials = new UserCredentials($userName, $this->passwordEncoder->encodePassword($plainPassword));
        $this->userRepository->saveUser($credentials);
    }
}