<?php

namespace App\Repository;

use App\Model\UserCredentials;

class RepositoryContainer implements UserRepositoryInterface
{
    /**
     * @var array
     */
    private $repositories = [];

    /**
     * @param UserRepositoryInterface $repository
     */
    public function addRepository(UserRepositoryInterface $repository)
    {
        $this->repositories[] = $repository;
    }

    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        foreach ($this->repositories as $repository) {
            $credentials = $repository->findCredentialsByUsername($username);

            if ($credentials instanceof UserCredentials) {
                return $credentials;
            }
        }

        return null;
    }

    /**
     * @param UserCredentials $credentials
     */
    public function saveUser(UserCredentials $credentials): void
    {
        foreach ($this->repositories as $repository) {
            $repository->saveUser($credentials);
        }
    }
}