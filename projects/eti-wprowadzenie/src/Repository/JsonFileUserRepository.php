<?php

namespace App\Repository;

use App\Model\UserCredentials;
use App\Security\Sha1PasswordEncoder;

class JsonFileUserRepository implements UserRepositoryInterface
{

    /**
     * test3 => Ktosiek
     * Maciek => test456
     *
     */
    /**
     * @var array
     */
    private $users;

    public function __construct(string $filename)
    {
        //TODO: implement this
        //TODO: instead of fopen, you can use: file_get_contents(), json_decode()
    }


    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        return null;
        if (!isset($this->users[$username])) {
            return null;
        }

        return new UserCredentials($username, $this->users[$username]);
    }

    public function saveUser(UserCredentials $credentials): void
    {
        // TODO: Implement saveUser() method.
    }
}