<?php

namespace App\Repository;


use App\Model\UserCredentials;
use PDO;

class PdoUserRepository implements UserRepositoryInterface
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct(string $dns, string $userName = null, string $password = null)
    {
        $this->pdo = new PDO($dns, $userName, $password);
    }

    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        $sql = "SELECT * FROM users WHERE username = :username";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            return null;
        }
        return new UserCredentials($row['username'], $row['password']);
    }

    public function saveUser(UserCredentials $credentials): void
    {
        $username = $credentials->getUsername();
        $endocdedPass = $credentials->getPassword();
        $sqlInsert = "INSERT INTO users (username, password) VALUES(?,?);";
        $stmt = $this->pdo->prepare($sqlInsert);

        $stmt->execute([$username, $endocdedPass]);
    }
}