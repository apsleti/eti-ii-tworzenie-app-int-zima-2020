<?php

namespace App\Repository;


use App\Model\UserCredentials;
use App\Security\Sha1PasswordEncoder;

class InMemoryUserRepository implements UserRepositoryInterface
{
    /**
     * @var array
     *
     * np. $users = [
     *  'username1' => 'encoded_password1',
     *  'Arek' => 'encoded_test123'
     * ]
     */
    private $users;


    /**
     * InMemoryUserRepository constructor.
     * @param array $users
     * @param Sha1PasswordEncoder $encoder
     */
    public function __construct(array $users = [], Sha1PasswordEncoder $encoder)
    {
        foreach ($users as $username => $password) {
            $this->users[$username] = $encoder->encodePassword($password);
        }
    }


    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        if (!isset($this->users[$username])) {
            return null;
        }

        return new UserCredentials($username, $this->users[$username]);
    }

    public function saveUser(UserCredentials $credentials): void
    {
        // TODO: Implement saveUser() method.
    }
}