<?php

namespace App\Repository;

use App\Model\UserCredentials;
use App\Security\Sha1PasswordEncoder;

class CsvFileUserRepository implements UserRepositoryInterface
{
    /**
     * @var array
     */
    private $users;

    public function __construct(string $filename)
    {
        $this->users = [];
        if ($file = fopen($filename, "r")) {
            while (($data = fgetcsv($file, 0, ";"))) {
                $this->users[$data[0]] = $data[1];
            }
            fclose($file);
        }
    }


    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        if (!isset($this->users[$username])) {
            return null;
        }

        return new UserCredentials($username, $this->users[$username]);
    }

    public function saveUser(UserCredentials $credentials): void
    {
        // TODO: Implement saveUser() method.
    }
}