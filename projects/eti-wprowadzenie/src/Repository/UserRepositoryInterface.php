<?php

namespace App\Repository;

use App\Model\UserCredentials;

/**
 * Interface UserRepositoryInterface
 */
interface UserRepositoryInterface
{
    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials;

    /**
     * @param UserCredentials $credentials
     */
    public function saveUser(UserCredentials $credentials): void;
}