<?php

namespace App;

class Request
{
    /**
     * @var array
     */
    private $pathParameters = [];

    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $queryParameters;

    /**
     * @var string
     */
    private $method = 'GET';

    /**
     * @var array
     */
    private $post = [];

    /**
     * Request constructor.
     * @param string $path
     * @param string $method
     * @param array $queryParameters
     * @param array $post
     */
    public function __construct(string $path, string $method, array $queryParameters, array $post)
    {
        $this->path = $path;
        $this->queryParameters = $queryParameters;
        $this->method = $method;
        $this->post = $post;
    }

    /**
     * @return Request
     */
    public static function prepareRequest()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $index = strpos($uri, '?');

        if ($index === false) {
            $path = $uri;
        } else {
            $path = substr($uri, 0, $index);
        }

        return new self($path, $_SERVER['REQUEST_METHOD'], $_GET, $_POST);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasQueryParam(string $name)
    {
        return isset($this->queryParameters[$name]);
    }

    /**
     * @param string $name
     * @param null $default
     * @return mixed|null
     */
    public function getQueryParam(string $name, $default = null)
    {
        return $this->queryParameters[$name] ?? $default;
    }

    /**
     * @return array
     */
    public function getPathParameters(): array
    {
        return $this->pathParameters;
    }

    /**
     * @param $name
     * @param $default
     * @return mixed|null
     */
    public function getParameter($name, $default = null)
    {
        return $this->pathParameters[$name] ?? $default;
    }

    /**
     * @param $params
     */
    public function setParameters($params)
    {
        $this->pathParameters = $params;
    }

    public function getPost(string $name, $default = null)
    {
        return $this->post[$name] ?? $default;
    }

    public function isPost()
    {
        return strtoupper($this->method) === 'POST';
    }
}