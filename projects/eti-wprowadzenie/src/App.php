<?php

namespace App;

use App\Controllers\ControllerInterface;
use App\Exception\PageNotFoundException;
use App\Exception\ServerErrorException;
use App\Response\ErrorResponse;

class App
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @throws \Exception
     */
    public function run(): void
    {
        $this->request = Request::prepareRequest();
        /** @var Router $router */
        $router = ServiceContainer::getInstance()->get('router');

        try {
            $matchedRoute = $router->match($this->request);
            $response = $matchedRoute($this->request);
        } catch (PageNotFoundException $exception) {
            $response = new ErrorResponse($router, $exception, 404);
        } catch (ServerErrorException $exception) {
            $response = new ErrorResponse($router, $exception, 500);
        }


        foreach ($response->getHeaders() as $header) {
            header($header);
        }
        echo $response->getBody();
    }
}