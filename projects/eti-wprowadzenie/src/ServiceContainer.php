<?php

namespace App;

use App\Controllers\LoginCheckController;
use App\Controllers\LogoutController;
use App\Controllers\PageController;
use App\Controllers\RegisterController;
use App\Repository\CsvFileUserRepository;
use App\Repository\JsonFileUserRepository;
use App\Repository\PdoUserRepository;
use App\Repository\RepositoryContainer;
use App\Security\Sha1PasswordEncoder;
use App\Services\UserManager;
use App\Session\Session;

/**
 * Class ServiceContainer
 */
class ServiceContainer
{
    private static $instance;

    private $services;

    /**
     * ServiceContainer constructor.
     */
    private function __construct()
    {
        $this->services['router'] = function () {

            $router = new Router();

            $router->addRoute('homepage', [
                'path' => '/',
                'controller' => function () {
                    return new PageController('homepage');
                }
            ]);

            $router->addRoute('blog', [
                'path' => '/blog/{id}',
                'controller' => function () {
                    return new PageController('blog');
                }
            ]);

            $router->addRoute('user', [
                'path' => '/user',
                'controller' => function () {
                    return new PageController('user');
                }
            ]);

            $router->addRoute('register', [
                'path' => '/register',
                'controller' => function () use ($router) {
                    return new RegisterController($router, $this->get('user_manager'));
                }
            ]);

            $router->addRoute('login_check', [
                'path' => '/login-check',
                'controller' => function () {
                    return new LoginCheckController(
                        $this->get('session'),
                        $this->get('router'),
                        $this->get('user_repository'),
                        $this->get('password_encoder'));
                }
            ]);

            $router->addRoute('logout', [
                'path' => '/logout',
                'controller' => function () {
                    return new LogoutController($this->get('session'), $this->get('router'));
                }
            ]);

            $router->addRoute('invalid', [
                'path' => '/invalid'
            ]);

            return $router;
        };

        $this->services['session'] = function () {
            return new Session();
        };

        $this->services['password_encoder'] = function () {
            return new Sha1PasswordEncoder();
        };

        $this->services['user_repository'] = function () {
            $repository = new RepositoryContainer();
            $repository->addRepository(new PdoUserRepository('sqlite:///' . __DIR__ . '/../data/users.db'));
            //$repository->addRepository(new CsvFileUserRepository(__DIR__ . '/../data/users.csv'));
            //$repository->addRepository(new JsonFileUserRepository(__DIR__ . '/../data/users.json'));

            return $repository;
        };

        $this->services['user_manager'] = function () {
            return new UserManager(
                $this->get('user_repository'),
                $this->get('password_encoder')
            );
        };
    }

    /**
     * @return ServiceContainer
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        if (!isset($this->services[$id])) {
            throw new \Exception(sprintf('Service "%s" is not defined.', $id));
        }

        return $this->services[$id]();
    }
}