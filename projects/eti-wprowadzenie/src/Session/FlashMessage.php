<?php

namespace App\Session;

class FlashMessage
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var array
     */
    private $messages = [];

    /**
     * FlashMessage constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->messages = unserialize($session->get('flash_messages'));
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        $messages = $this->messages;
        $this->messages = [];
        $this->store();;

        return $messages;
    }

    /**
     * @param string $type
     * @param string $message
     */
    public function setMessage(string $type, string $message): void
    {
        $this->messages[$type][] = $message;
        $this->store();
    }

    public function checkMessages()
    {
        return $this->messages;
    }

    private function store()
    {
        $this->session->set('flash_messages', serialize($this->messages));
    }
}