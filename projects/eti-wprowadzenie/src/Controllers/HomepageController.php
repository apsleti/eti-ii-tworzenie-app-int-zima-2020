<?php

namespace App\Controllers;

use App\Request;
use App\Response\JsonResponse;
use App\Response\Response;

/**
 * Class HomepageController
 */
class HomepageController implements ControllerInterface
{
    /**
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        return new JsonResponse([
                'Homepage Test Response',
                'param1' => 123
            ]
        );
    }
}