<?php

namespace App\Controllers;

use App\Request;
use App\Response\LayoutResponse;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\ServiceContainer;
use App\Session\Session;

/**
 * Class LogoutController
 */
class LogoutController implements ControllerInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * LoginCheckController constructor.
     * @param Session $session
     * @param Router $router
     */
    public function __construct(Session $session, Router $router)
    {
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request): Response
    {
        $response = new RedirectResponse(
            $this->router->generate('homepage')
        );

        $this->session->destroy();

        $this->session->start();
        $this->session->setFlashMessage('success', "Udało Ci się wylogować.");

        return $response;
    }
}