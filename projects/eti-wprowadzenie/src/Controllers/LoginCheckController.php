<?php

namespace App\Controllers;

use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Security\Sha1PasswordEncoder;
use App\Session\Session;
use App\Repository\RepositoryContainer;

/**
 * Class LoginCheckController
 *
 */
class LoginCheckController implements ControllerInterface
{
    const POST_USERNAME = 'username';
    const POST_PASSWORD = 'password';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var RepositoryContainer
     */
    private RepositoryContainer $repository;
    /**
     * @var Sha1PasswordEncoder
     */
    private Sha1PasswordEncoder $passwordEncoder;

    /**
     * LoginCheckController constructor.
     * @param Session $session
     * @param Router $router
     * @param RepositoryContainer $repository
     * @param Sha1PasswordEncoder $passwordEncoder
     */
    public function __construct(Session $session, Router $router, RepositoryContainer $repository, Sha1PasswordEncoder $passwordEncoder)
    {
        $this->session = $session;
        $this->router = $router;
        $this->repository = $repository;
        $this->passwordEncoder = $passwordEncoder;
    }


    public function __invoke(Request $request): Response
    {
        $response = new RedirectResponse(
            $this->router->generate('homepage')
        );

        if (!$request->isPost()) {
            return $response;
        }

        $username = $request->getPost(self::POST_USERNAME);
        $plainPassword = $request->getPost(self::POST_PASSWORD);

        $password = $this->passwordEncoder->encodePassword($plainPassword);
        $userCredentials = $this->repository->findCredentialsByUsername($username);

        if ($userCredentials && $password == $userCredentials->getPassword()) {
            $this->session->regenerate();
            $this->session->set('user', $username);
            $this->session->setFlashMessage('success', "Udało Ci sie zalogować do systemu");
        } else {
            $this->session->setFlashMessage('error', "Podane dane logowania sia nieprawidłowe");
        }

        return $response;
    }
}