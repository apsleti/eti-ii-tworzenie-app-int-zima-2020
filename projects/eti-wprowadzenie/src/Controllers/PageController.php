<?php

namespace App\Controllers;

use App\Request;
use App\Layout;
use App\Response\LayoutResponse;
use App\Response\Response;
use App\ServiceContainer;

/**
 * Class PageController
 */
class PageController implements ControllerInterface
{
    /**
     * @var string
     */
    private $layout;

    /**
     * @var string;
     */
    private $name;

    /**
     * PageController constructor.
     * @param string $name
     * @param string $layout
     */
    public function __construct(string $name, string $layout = 'default')
    {
        $this->name = $name;
        $this->layout = $layout;
    }


    public function __invoke(Request $request): Response
    {
        $templateParams = [
            'request' => $request,
            'router' => ServiceContainer::getInstance()->get('router')
        ];

        return new LayoutResponse($this->name, $templateParams, $this->layout);
    }
}