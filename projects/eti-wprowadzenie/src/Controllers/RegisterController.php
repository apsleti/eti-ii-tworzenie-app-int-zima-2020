<?php

namespace App\Controllers;

use App\Exception\UserExistsExcetpion;
use App\Model\UserCredentials;
use App\Repository\RepositoryContainer;
use App\Request;
use App\Security\Sha1PasswordEncoder;
use App\Response\LayoutResponse;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Services\UserManager;

/**
 * Class RegisterController
 * @package App\Controllers
 */
class RegisterController implements ControllerInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var UserManager
     */
    private UserManager $userManager;

    /**
     * RegisterController constructor.
     * @param Router $router
     * @param UserManager $userManager
     */
    public function __construct(Router $router, UserManager $userManager)
    {
        $this->router = $router;
        $this->userManager = $userManager;
    }


    public function __invoke(Request $request): Response
    {
        $errors = [];
        $formData = [];
        if ($request->isPost()) {
            $formData = $this->getFormData($request);
            $errors = $this->validate($formData);

            if (count($errors) == 0) {
                try {
                    $this->userManager->registerUser($formData['username'], $formData['password']);
                    return new RedirectResponse($this->router->generate('homepage'));
                } catch (UserExistsExcetpion $e) {
                    $errors['username'][] = $e->getMessage();
                }
            }
        }

        return new LayoutResponse('register', [
            'request' => $request,
            'router' => $this->router,
            'errors' => $errors,
            'formData' => $formData
        ]);
    }

    /**
     * @param Request $request
     * @return mixed|null
     */
    private function getFormData(Request $request)
    {
        $formData = $request->getPost('registration', []);
        $allowedFields = ['username', 'password', 'repeatPassword'];
        foreach ($allowedFields as $field) {
            $formData[$field] = $this->cleanValue($formData, $field);
        }

        return $formData;
    }

    /**
     * @param $formData
     * @param string $field
     * @return string|null
     */
    private function cleanValue(array $formData, string $field)
    {
        $value = trim($formData[$field] ?? '');

        return $value !== '' ? $value : null;
    }

    /**
     * @param array $formData
     * @return array
     */
    private function validate(array $formData)
    {
        $errors = [];
        $username = $formData['username'];
        $password = $formData['password'];
        $repeatPassword = $formData['repeatPassword'];

        if (!$username) {
            $errors['username'][] = 'Nazwa użytkownika jest wymagana!';
        }

        if (!$password) {
            $errors['password'][] = 'Hasło jest wymagane!!';
        }

        if (!$repeatPassword) {
            $errors['repeatPassword'][] = 'Hasło jest wymagane!!';
        }

        if ($password !== $repeatPassword) {
            $errors['repeatPassword'][] = 'Hasła muszą być takie same!';
        }

        return $errors;
    }
}