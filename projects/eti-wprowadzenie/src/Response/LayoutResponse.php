<?php

namespace App\Response;

use App\Layout;

class LayoutResponse extends Response
{
    public function __construct(string $name, array $params = [], string $layoutName = 'default', array $headers = [], int $status = 200)
    {
        $layout = new Layout($name, $layoutName, $params);
        parent::__construct($layout->render(), $headers, $status);
    }
}