<?php

namespace App\Response;

use App\Router;

class ErrorResponse extends LayoutResponse
{

    public function __construct(Router $router, $exception, $errorCode)
    {
        $page = sprintf('errors/%d', $errorCode);
        $params = [
            'router' => $router,
            'exception' => $exception
        ];

        parent::__construct($page, $params, 'error', [], $errorCode);
    }
}